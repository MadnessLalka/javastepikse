public class ConditionalOperator1 {
    public static void main(String[] args) {
        int money = 2;

        if (money > 10) {
            System.out.println("pizza");
        } else if (money >= 6 && money <= 10) {
            System.out.println("hamburger");
        } else if (money < 6) {
            System.out.println("sandwich");
        }
    }
}
